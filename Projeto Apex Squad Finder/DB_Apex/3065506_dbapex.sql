-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: fdb20.awardspace.net
-- Generation Time: Jun 13, 2019 at 02:00 AM
-- Server version: 5.7.20-log
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `3065506_dbapex`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_clientes`
--

CREATE TABLE `tb_clientes` (
  `id` int(11) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `email` varchar(50) NOT NULL,
  `originid` varchar(20) NOT NULL,
  `senha` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_clientes`
--

INSERT INTO `tb_clientes` (`id`, `nome`, `email`, `originid`, `senha`) VALUES
(7, 'sdsdsd', 'sdsdsadsd@gmail.com', 'asdsds', 'sdsds'),
(15, 'felipe', 'felipe@felipe1.com', 'DZO', '123'),
(11, 'fernando', 'fernando@gmail.com', 'fernando12369', 'asd123'),
(13, 'Meupau', 'meupau@gmail.com', 'Meupau', '1234'),
(1, 'jose', 'jcdjnilton196@gmail.com', 'Niwzera', '123'),
(5, '12334', '1234@gmail.com', 'sadsd', 'dsad'),
(19, 'teste1', 'teste1@teste.com', 'sdsd', '123'),
(3, 'dsdsdsad', 'sdsasdsad@gmai.com', 'sdsdsad', 'sdsad'),
(9, 'teste', 'teste@gmail.com', 'Test', 'teste123'),
(17, 'teste1', 'teste@teste1.com', 'teste', '123');

-- --------------------------------------------------------

--
-- Table structure for table `tb_registro`
--

CREATE TABLE `tb_registro` (
  `OriginID` varchar(20) NOT NULL,
  `id` int(20) NOT NULL,
  `Plataforma` varchar(8) NOT NULL,
  `Personagem` varchar(17) NOT NULL,
  `Nivel` varchar(10) NOT NULL,
  `Comunicacao` varchar(10) NOT NULL,
  `InformacaoExtra` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_registro`
--

INSERT INTO `tb_registro` (`OriginID`, `id`, `Plataforma`, `Personagem`, `Nivel`, `Comunicacao`, `InformacaoExtra`) VALUES
('DZO', 45, 'PC', 'Bangalore', '61-80', 'Jogando04', 'Vou jogar de wraith'),
('Niwzera', 44, 'PC', 'Octane', '81-100', 'Jogando20', 'Saudade do que a gente nÃ£o viveu ainda, bb ;( VAMOS JOGAR?'),
('Test', 47, 'Ps4', 'Octane', '81-100', 'Jogando05', 'PreferÃªncia de bangalore e lifeline, lvl 80+ gogogo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  ADD PRIMARY KEY (`originid`) USING BTREE,
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `id unique` (`id`) USING BTREE;

--
-- Indexes for table `tb_registro`
--
ALTER TABLE `tb_registro`
  ADD PRIMARY KEY (`OriginID`),
  ADD UNIQUE KEY `Unico` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_registro`
--
ALTER TABLE `tb_registro`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
