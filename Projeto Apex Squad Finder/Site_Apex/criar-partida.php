<?php
        // iniciar uma sessão
        session_start(); 
        if(isset($_SESSION['user'])){
                ?>
                
<html>
<head>
	<title>AsF - Criar Partida</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/criar.css">
	<link rel="icon" href="Imagens/apx2.png" type="image/x-icon" />
</head>

<body>
								<!--COFIGURAÇÃO ~HEADER -->
				<header>
							<div id="cabecalho">
						
						<div id="cabecalho_logo">
							<a href="index.php"> APEX SQUAD FINDER</a>
						</div>

						<ul id="cabecalho_menu">
                                                        <li><a href="suporte.php">Como utilizar</a></li>
                                                        <li><a href="sobre.php">Sobre</a></li>
                                                        <li><a href="cadastro/consultas.php"><?= $_SESSION['idorigin'] ?></a></li>
						</ul>
						
					</div>
	

</header>
				<main>

									<div> <!--   DIV LOGO -->
					<img id="imgmenustye" src="Imagens/ApexCriar.png">
		</div>
						
						<div id="principal">
							
							<div id="criarpartidatxt">
								<h1>CRIAR PARTIDA</h1>
							</div>

							<div id="menuCriarpartida">


								<div class="config">							
							Origin ID
		</div>

						<div class="config2">							
							Plataforma
		</div>

						<div class="config">							
							Personagem
		</div>

						<div class="config">							
							Nível
		</div>
		<br>

					<form method="post" action="cadastro/banco_de_dados/create_registro.php">
						
						<div class="config-id">	
							
							<input type="text" name="OriginID" maxlength="30" style="width: 110px; outline: none;" value="<?= $_SESSION['idorigin'] ?>" readonly placeholder="O msm registrado">							
							
		</div>

								<div class="config-plat">
						<select name="plat" style="width: 90px" required> 

							<option>PC</option>		
							<option>Ps4</option>
							<option>Xbox</option>

						</select>							
							
		</div>

						<div class="config-pers">	
						<select name="pers" required> 

							<option>Bangalore</option>		
							<option>Bloodhound</option>
							<option>Caustic</option>
							<option>Gibraltar</option>
							<option>Lifeline</option>
							<option>Mirage</option>
							<option>Octane</option>
							<option>Pathfinder</option>
							<option>Wraith</option>

						</select>							
							
		</div>

						<div class="config-nv">
						<select name="nv" required> 

							<option>1-20</option>		
							<option>21-40</option>
							<option>41-60</option>
							<option>61-80</option>
							<option>81-100</option>

						</select>							
							
		</div>

		<br>

						<div class="config-inf">	

							<p style="padding-bottom: 10px">Informação extra</p>
							
		</div>

						<div class="config-comun">	

							<p style="padding-bottom: 10px">Sala no Discord</p>
							
		</div>
		<br>
						<div class="config-areainf">
							<textarea rows="4" cols="40" name = "areainf" maxlength="70" style="resize: none;" placeholder="Escreva uma informação extra                          exemplo: Quero jogar com uma Lifeline e uma Wraith" required></textarea>	
						</div>

						<div class="config-areacomu">
									
								<select name="comu" size="5" style="padding:5px 5px 5px 5px; margin-left:10px;" required> 

									<option value="Jogando01">Jogando01</option>
									<option value="Jogando02">Jogando02</option>
									<option value="Jogando03">Jogando03</option>
                                                                        <option value="Jogando04">Jogando04</option>
                                                                        <option value="Jogando05">Jogando05</option>
                                                                        <option value="Jogando06">Jogando06</option>
                                                                        <option value="Jogando07">Jogando07</option>
                                                                        <option value="Jogando08">Jogando08</option>
                                                                        <option value="Jogando09">Jogando09</option>
                                                                        <option value="Jogando10">Jogando10</option>
                                                                        <option value="Jogando11">Jogando11</option>
                                                                        <option value="Jogando12">Jogando12</option>
                                                                        <option value="Jogando13">Jogando13</option>
                                                                        <option value="Jogando14">Jogando14</option>
                                                                        <option value="Jogando15">Jogando15</option>
                                                                        <option value="Jogando16">Jogando16</option>
                                                                        <option value="Jogando17">Jogando17</option>
                                                                        <option value="Jogando18">Jogando18</option>
                                                                        <option value="Jogando19">Jogando19</option>
                                                                        <option value="Jogando20">Jogando20</option>

								</select>
						</div>
						<br>


								<div class="config-bt">

							<button type="submit" id="buttonstyle">CRIAR</button>						
							
		</div>

		</form>
						<div style="clear: both;">
							
						</div>


	


							</div>



				</div>

</main>

								<!--COFIGURAÇÃO FOOTER -->
				<footer>

			<div id="rodape">

				<div id="listarodape">
					<ul id="listtype">
						<li ><a href="Politica_de_privacidade.php">Política de privacidade</a></li>
						<li><a href="politica_de_cookies.php">Política de cookies</a></li>
						<li><a href="termos_de_servico.php">Termos de Serviço</a></li>
					</ul>

				</div>
				
				<p>Todos os direitos reservados</p>
				<p>Copyright © 2019 de Apex Squad Finder Team</p>
			</div>
	

</footer>


</body>
</html>
<?php }else{?>
                <script> location.replace("cadastro/login.php"); </script>
<?php }?>