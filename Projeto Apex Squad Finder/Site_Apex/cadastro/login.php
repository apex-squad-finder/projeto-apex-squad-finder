<?php
        // iniciar uma sessão
        session_start(); 
        if(isset($_SESSION['user'])){
                ?>                
                <script> location.replace("../index.php"); </script>
<?php }else{?>


<head>
	<title>AsF-Login</title>
	<meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="../CSS/sobre.css">
	<link rel="icon" href="Imagens/apx2.png" type="image/x-icon" />
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
      <!--CSS DO MATERIALIZE-->
      <link rel="stylesheet" href="materialize/css/materialize.min.css">
      
      
</head> 

				<header>
							<div id="cabecalho">
						
						<div id="cabecalho_logo">
							<a href="../index.php"> APEX SQUAD FINDER</a>
						</div>

						<ul id="cabecalho_menu">
							<li><a href="../index.php">Home</a></li>
							<li><a href="../suporte.php">Suporte</a></li>
							<li><a href="../sobre.php">Sobre</a></li>
                                                        <li><a href="index.php">Cadastro</a></li>
						</ul>
						
					</div>
	

</header>

				<div> <!--   DIV LOGO -->
					<img id="imgmenustye" src="Imagens/ApexCriar.png">
		</div>


 <div class="row container" >
                    
                        <fieldset class="formulario">
                            <legend><img src="Imagens/apx2.png" alt="(imagem)"width="100"></legend>
                            <h5 class="light center">Login</h5>
<body style="color: white; background-image: url(../Imagens/fundocriar2.png);">
            <!--Arquivos Jquery e JavaScrifpt-->
            <script type="text/javascript" src="materialize/js/jquery-3.4.1.min.js"></script>
            <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
            
            <!-- Inicialização Jquery-->
            <script type="text/javascript">
                $(document).ready(function(){
                    
                });
            </script>

<section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <div class="notification is-danger">
                    </div>
                    <div class="box">
                        <form class="form-signin" action="banco_de_dados/loginvalida.php" method="POST">
                            <div class="field">
                                
                                 <!--Campo email-->
                            <div class="input-field col s12">
                                <i class="material-icons prefix">email</i>
                                <input type="email" name="email" id="email" maxlength="50" required >
                                <label for="email">Email do Perfil</label>
                                    
                            </div>
                            </div>

                            <!--Campo senha-->
                           <div class="input-field col s12">
                               <i   class="material-icons prefix">vpn_key</i>
                               <input type="password" name="senha" id="senha" maxlength="25" required>
                               <label for="senha">Senha</label>
                            </div>
                            <p class="has-text-centered has-text-grey">
                                    <?php if(isset($_SESSION['loginErro'])){
                                        echo $_SESSION['loginErro'];
                                        unset($_SESSION['loginErro']);                                        
                                }?>
                            </p>
                            <input type="submit" value="entrar" class="btn blue">                        </form>
                            </div>
                            
                    </div>
                </div>
            </div>
        </div>
    </section>
    
		<footer>
	

			<div id="rodape" style="margin-top: 200px">

				<div id="listarodape">
					<ul id="listtype">
						<li ><a href="Politica_de_privacidade.php">Política de privacidade</a></li>
						<li><a href="politica_de_cookies.php">Política de cookies</a></li>
						<li><a href="termos_de_servico.php">Termos de Serviço</a></li>
					</ul>

				</div>
				
				<p>Todos os direitos reservados</p>
				<p>Copyright © 2019 de Apex Squad Finder Team</p>
			</div>


</footer>
</body>

</html>

<?php }?>
