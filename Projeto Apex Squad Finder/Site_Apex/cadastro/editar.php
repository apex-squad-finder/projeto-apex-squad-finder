<?php 
include_once 'includes/header.inc.php';
include_once 'includes/menu.inc.php';
?>

<div class="row container">
    <div class="col s12">
        <h5 class="light">Edição de perfil</h5><hr>
        
    </div>
</div>
<?php
    include_once 'banco_de_dados/conexao.php';
    $id = filter_input(INPUT_GET, 'id',FILTER_SANITIZE_NUMBER_INT);
    $querySelect = $connect->query("select * from tb_clientes where id = '$id'");
    
    while($registros = $querySelect->fetch_assoc()):
        $id = $registros['id'];
        $nome = $registros['nome'];
        $email = $registros['email'];
        $originid = $registros['originid'];
        
    endwhile;
?>

<!--Formulario de cadastro-->
                <div class="row container" >
                    
                    <form action="banco_de_dados/update.php" method="post" class="col s12">
                        <fieldset class="formulario">
                            <legend><img src="Imagens/apx2.png" alt="(imagem)"width="100"></legend>
                            <h5 class="light center">Alteração de perfil</h5>
                      
                            <!--Campo nome-->
                            <div class="input-field col s12">
                                <i class="material-icons prefix">account_circle</i>
                                <input type="text" name="nome" id="nome" value="<?php echo $nome?>" maxlength="40" required autofocus>
                                <label for="nome">Nome do Perfil</label>
                                    
                            </div>
                            
                            <!--Campo email-->
                            <div class="input-field col s12">
                                <i class="material-icons prefix">email</i>
                                <input type="email" name="email" id="email" value="<?php echo $email?>" maxlength="50" required >
                                <label for="email">Email do Perfil</label>
                                    
                            </div>
                            <!--Campo OrigiID-->
                            <div class="input-field col s12">
                                <i class="material-icons prefix">account_circle</i>
                                <input type="text" name="originid" id="originid" value="<?php echo $originid?>" maxlength="30" required >
                                <label for="originid">Origin ID do Perfil</label>
                                    
                            </div>
                            <!--Campo senha-->


                           <div class="input-field col s12">
                               <i   class="material-icons prefix">vpn_key</i>
                               <input type="password" name="senha" id="senha" value="<?php echo $senha?>" maxlength="25" required>
                               <label for="senha">Senha</label>
                            </div>
                            
                            <!--Botoes-->
                            <div class="input-field col s12">
                                <input type="submit" value="alterar" class="btn blue">
                                <a href="consultas.php" class="btn red">Cancelar</a>
                            </div>
                            
                        </fieldset>
                    </form>
                    
                    
                </div>

<?php include_once 'includes/footer.inc.php'?>