<?php
        // iniciar uma sessão
        session_start(); 
        if(isset($_SESSION['user'])){ 
                include_once 'banco_de_dados/conexao.php';
                ?>


<html>
<head>
	<title>AsF - Perfil</title>
        <meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../CSS/index.css">
	<link rel="icon" href="Imagens/apx2.png" type="image/x-icon" />
</head>

<body>

		<header>
			
			<div id="cabecalho" >
				
				<div id="cabecalho_logo">
					<a href="../index.php"> APEX SQUAD FINDER</a>
				</div>

				<ul id="cabecalho_menu">
					<li><a href="../suporte.php">Suporte</a></li>
					<li><a href="../sobre.php">Sobre</a></li>
                                        <li><a href="consultas.php"><?= $_SESSION['idorigin'] ?></a></li>
                                                                               
				</ul>
				
			</div>
</header>


<main>

				<div> <!--   DIV LOGO -->
					<img id="imgmenustye" style="margin-bottom: 20px" src="../Imagens/ApexCriar.png">
		</div>


				<div id="principal"> <!--   DIV BACKGROUND PRINCIPAL WHITE -->


<?php

				
				$querySelect = $connect->query("select * from tb_clientes");
				while($registros = $querySelect->fetch_assoc()):          
                                    $id = $registros['id'];
				    $nome=$registros['nome'];
				    $email = $registros['email'];
				    $originid = $registros['originid'];
                                    
                                    if($email == $_SESSION['user']){
                                    ?>                            

				<div id="resultado-box" style="color: white;"> 
                                <h5 class="light" style="text-align: center; font-size: 2.3em;">Suas Informações</h5><hr>
                                
                                <ul style="padding: 20px; list-style-type: none;">
                                        <li style="padding-bottom: 15px"><h3>Nome:      <?= $nome?>           </h3></li>
                                        <li style="padding-bottom: 15px"><h3>Email:     <?= $email?>          </h3></li>
                                        <li style="padding-bottom: 15px"><h3>Origin ID: <?= $originid?>      </h3></li>
                                        
                                </ul>
                                <div style="display: inline-block; padding: 15px; float: right;">
                                       <a href='../logout.php'><input type=button name=logout value= logout ></a> 
                                </div>
                                <div style="clear: both;">
                                </div>
                                                                    
				</div> <!-- FIM DIV RESULTADO BOX  -->					
						

</div>

</main>

		<footer>
	

			<div id="rodape" style="margin-top: 200px">

				<div id="listarodape">
					<ul id="listtype">
						<li ><a href="../Politica_de_privacidade.php">Política de privacidade</a></li>
						<li><a href="../politica_de_cookies.php">Política de cookies</a></li>
						<li><a href="../termos_de_servico.php">Termos de Serviço</a></li>
					</ul>

				</div>
				
				<p>Todos os direitos reservados</p>
				<p>Copyright © 2019 de Apex Squad Finder Team</p>
			</div>


</footer>


</body>
</html>
<?php }
endwhile; }else{

 echo "<script>location.href='cadastro/login.php'";
        

}

?>