<?php
        // iniciar uma sessão
        session_start(); 
        if(isset($_SESSION['user'])){
                ?>
<html>
<head>
	<title>AsF - Suporte</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/suporte.css">
	<link rel="icon" href="Imagens/apx2.png" type="image/x-icon" />
</head>

<body>
								<!--COFIGURAÇÃO ~HEADER -->
				<header>
							<div id="cabecalho">
						
						<div id="cabecalho_logo">
							<a href="index.php"> APEX SQUAD FINDER</a>
						</div>

						<ul id="cabecalho_menu">
							<li><a href="index.php">Home</a></li>
							<li><a href="sobre.php">Sobre</a></li>
                                                        <li><a href="cadastro/consultas.php"><?= $_SESSION['idorigin'] ?></a></li>

						</ul>
						
					</div>
	

</header>
				<main>
								<div> <!--   DIV LOGO -->
									<img id="imgmenustye" style="margin-bottom: 20px" src="Imagens/ApexCriar.png">
								</div>
						
						<div id="principal" style="padding: 10px; color: white;">
							<h1>Como utilizar</h1>
                                                        <p>Para utilizar nosso site com 100% de eficiencia o usuário precisa ter um registro e entrar em nosso Discord.</p><br>
                                                        <h3>Como criar partida:</h3>
                                                        <p>Após o usuário ter criado sua conta será possível a criação de partidas. Para criar a partida
                                                        basta ir na página inicial e clicar em "criar partida", após ser redirecionado para a página de criação
                                                        o usuário terá q fornecer algumas informações, como: OriginID(tendo que ser o mesmo ID registrado), a 
                                                        plataforma na qual está jogando, o personagem que irá utilizar para jogar, informações extras caso seja
                                                        necessário e selecionar qual a forma de comunicação.</p><br>
                                                        <p style="font-size 0.6em;"><i>"O jogador que criar a sala deve obrigatoriamente entrar no discord do site,
                                                        para que assim os usuários que desejarem jogar com o mesmo possam entrar em contato."</i></p>
                                                        <br>
                                                        <p>Após a criação da partida o usuário deve aguardar o contato de outros usuários no discord.</p>
                                                        <p>Ao encontrar os jogadores desejados e a sala estiver completa, o usuário que criou a sala deve a excluir.</p>
							<br>
                                                        <h3>Entre em nosso <a href="https://discord.gg/5tGEWZA" style="color:white;"><b>Discord!!</b></a></h3><br>
                                                        <h1>Dúvidas?</h1>
														<p>Mande sua dúvida em nosso Discord.<br>Ou nos mande um email: <strong>apexsquadfinder@gmail.com</strong></p>
														<br>


				</div>







</main>

								<!--COFIGURAÇÃO FOOTER -->
				<footer>

			<div id="rodape">

				<div id="listarodape">
					<ul id="listtype">
						<li ><a href="Politica_de_privacidade.php">Política de privacidade</a></li>
						<li><a href="politica_de_cookies.php">Política de cookies</a></li>
						<li><a href="termos_de_servico.php">Termos de Serviço</a></li>
					</ul>

				</div>
				
				<p>Todos os direitos reservados</p>
				<p>Copyright © 2019 de Apex Squad Finder Team</p>
			</div>
	

</footer>


</body>
</html>

<?php }else{?>

<html>
<head>
	<title>AsF - Suporte</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/suporte.css">
	<link rel="icon" href="Imagens/apx2.png" type="image/x-icon" />
</head>

<body>
								<!--COFIGURAÇÃO ~HEADER -->
				<header>
							<div id="cabecalho">
						
						<div id="cabecalho_logo">
							<a href="index.php"> APEX SQUAD FINDER</a>
						</div>

						<ul id="cabecalho_menu">
							<li><a href="index.php">Home</a></li>
							<li><a href="sobre.php">Sobre</a></li>
							<li><a href="cadastro/index.php">Cadastro</a></li>
							<li><a href="cadastro/login.php">Login</a></li>

						</ul>
						
					</div>
	

</header>
				<main>
								<div> <!--   DIV LOGO -->
									<img id="imgmenustye" style="margin-bottom: 20px" src="Imagens/ApexCriar.png">
								</div>
						
						<div id="principal" style="padding: 10px; color: white;">
							<h1>Como utilizar</h1>
                                                        <p>Para utilizar nosso site com 100% de eficiencia o usuário precisa ter um registro e entrar em nosso Discord.</p><br>
                                                        <h3>Como criar partida:</h3>
                                                        <p>Após o usuário ter criado sua conta será possível a criação de partidas. Para criar a partida
                                                        basta ir na página inicial e clicar em "criar partida", após ser redirecionado para a página de criação
                                                        o usuário terá q fornecer algumas informações, como: OriginID(tendo que ser o mesmo ID registrado), a 
                                                        plataforma na qual está jogando, o personagem que irá utilizar para jogar, informações extras caso seja
                                                        necessário e selecionar qual a forma de comunicação.</p><br>
                                                        <p style="font-size 0.6em;"><i>"O jogador que criar a sala deve obrigatoriamente entrar no discord do site,
                                                        para que assim os usuários que desejarem jogar com o mesmo possam entrar em contato."</i></p>
                                                        <br>
                                                        <p>Após a criação da partida o usuário deve aguardar o contato de outros usuários no discord.</p>
                                                        <p>Ao encontrar os jogadores desejados e a sala estiver completa, o usuário que criou a sala deve a excluir.</p>
							<br>
                                                        <h3>Entre em nosso <a href="https://discord.gg/5tGEWZA" style="color:white;"><b>Discord!!</b></a></h3><br>
                                                        <h1>Dúvidas?</h1>
														<p>Mande sua dúvida em nosso Discord.<br>Ou nos mande um email: <strong>apexsquadfinder@gmail.com</strong></p>
														<br>
								

				</div>







</main>

								<!--COFIGURAÇÃO FOOTER -->
				<footer>

			<div id="rodape">

				<div id="listarodape">
					<ul id="listtype">
						<li ><a href="Politica_de_privacidade.php">Política de privacidade</a></li>
						<li><a href="politica_de_cookies.php">Política de cookies</a></li>
						<li><a href="termos_de_servico.php">Termos de Serviço</a></li>
					</ul>

				</div>
				
				<p>Todos os direitos reservados</p>
				<p>Copyright © 2019 de Apex Squad Finder Team</p>
			</div>
	

</footer>


</body>
</html>
              <?php }?>